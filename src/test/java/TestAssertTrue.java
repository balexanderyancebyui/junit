import static org.junit.Assert.*;

import org.junit.Test;

public class TestAssertTrue {

	@Test
	public void test() {

		Main test = new Main();

		Boolean output = test.assertTrueTest();

		assertTrue(output);
	}

}