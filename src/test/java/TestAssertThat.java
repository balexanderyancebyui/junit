import static org.junit.Assert.*;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

public class TestAssertThat {

	@Test
	public void test() {

		Main test = new Main();

		Integer output = test.assertThatTest();

		assertThat(output, isA(Integer.class));
	}

}