import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class TestAssertSame {

	@Test
	public void test() {


		ArrayList<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i < 100; i++) {
			list.add((int)(Math.random() * 100));
		}

		Main test = new Main();

		ArrayList<Integer> output = test.assertSameTest(list);

		assertSame(list, output);
	}

}